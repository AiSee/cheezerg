package main

import (
	"bitbucket.org/aisee/minilog"
	"github.com/aiseeq/s2l/lib/scl"
	"github.com/aiseeq/s2l/protocol/api"
	"github.com/aiseeq/s2l/protocol/client"
	"github.com/google/gxui/math"
	"math/rand"
	"time"
)

// todo: не добывать газ против воркер-раша

func RunAgent(c *client.Client) {
	b := &bot{
		Bot: scl.New(c, nil),
	}
	b.FramesPerOrder = 3
	b.UnitCreatedCallback = b.OnUnitCreated
	b.LastLoop = -math.MaxInt
	b.MaxGroup = MaxGroup
	stop := make(chan struct{})
	b.Init(stop) // Called later because in Init() we need to use *B in callback

	for b.Client.Status == api.Status_in_game {
		b.Step()

		if _, err := c.Step(api.RequestStep{Count: uint32(b.FramesPerOrder)}); err != nil {
			if err.Error() == "Not in a game" {
				log.Info("Game over")
				return
			}
			log.Fatal(err)
		}

		b.UpdateObservation()
	}
	stop <- struct{}{}
}

func main() {
	log.SetConsoleLevel(log.L_info) // L_info L_debug
	rand.Seed(time.Now().UnixNano())

	// Create the agent and then start the game
	// client.SetRealtime()
	// client.SetMap(client.Maps2021season1[0] + ".SC2Map")
	myBot := client.NewParticipant(api.Race_Zerg, "CheeZerg")
	cpu := client.NewComputer(api.Race_Random, api.Difficulty_CheatMoney, api.AIBuild_RandomBuild)
	cfg := client.LaunchAndJoin(myBot, cpu)

	RunAgent(cfg.Client)
}
