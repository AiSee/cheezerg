package main

import (
	"github.com/aiseeq/s2l/lib/point"
	"github.com/aiseeq/s2l/lib/scl"
	"github.com/aiseeq/s2l/protocol/api"
	"github.com/aiseeq/s2l/protocol/enums/ability"
	"github.com/aiseeq/s2l/protocol/enums/buff"
	"github.com/aiseeq/s2l/protocol/enums/effect"
	"github.com/aiseeq/s2l/protocol/enums/protoss"
	"github.com/aiseeq/s2l/protocol/enums/terran"
	"github.com/aiseeq/s2l/protocol/enums/zerg"
	"math"
	"math/rand"
)

// todo: При строительстве шпилей не просто залезать в пространство атаки но и проверять дпс по зоне
// todo: разделение вражеских сил по группам и рассматривать их как разные опасности
// todo: group lings before attack
// todo: lings groups logic
// todo: better spines building

var creepPath point.Points
var takeExp = 2
var isRealtime = false
var workerRush = false
var safeMode = true
var armyAttack = false
var buildSpines = false

const (
	Miners scl.GroupID = iota + 1
	Builders
	WorkerRushDefenders
	ProxyBuilder
	Scout
	ScoutBase
	Overs
	Lings
	Queens
	Retreat
	HomeQueen
	HomeHatchery
	ExpHatchery
	ProxyHatchery
	UnderConstruction
	Buildings
	ActiveTumors
	UsedTumors
	MaxGroup
)
const safeBuildRange = 7

func (b *bot) OnUnitCreated(unit *scl.Unit) {
	if unit.UnitType == zerg.Drone {
		b.Groups.Add(Miners, unit)
		return
	}
	if unit.UnitType == zerg.Zergling {
		b.Groups.Add(Lings, unit)
		return
	}
	if unit.UnitType == zerg.Queen {
		if unit.Point().IsFurtherThan(5, b.Locs.MyStart) {
			// Add queens from proxy hat to an army
			b.Groups.Add(Queens, unit)
		} else {
			b.Groups.Add(HomeQueen, unit)
		}
		return
	}
	if unit.UnitType == zerg.Overlord {
		b.Groups.Add(Overs, unit)
		return
	}
	if unit.UnitType == zerg.CreepTumor || unit.UnitType == zerg.CreepTumorQueen {
		b.Groups.Add(ActiveTumors, unit)
		return
	}
	if unit.UnitType == zerg.Hatchery && unit.BuildProgress == 1 {
		// Ready from game start
		b.Groups.Add(HomeHatchery, unit)
		return
	}
	if unit.IsStructure() {
		if unit.BuildProgress < 1 {
			b.Groups.Add(UnderConstruction, unit)
			return
		}
	}
}

func (b *bot) BuildingsCheck() {
	buildings := b.Groups.Get(UnderConstruction).Units
	for _, building := range buildings {
		if building.BuildProgress == 1 {
			switch building.UnitType {
			case zerg.Hatchery:
				// Second hatchery should be proxy
				if b.Groups.Get(ProxyHatchery).Units.Empty() && b.Units.My[zerg.Hatchery].Filter(scl.Ready).Len() == 2 {
					b.Groups.Add(ProxyHatchery, building)

					// This will run once per new hatchery
					creepPath, _ = b.Path(building.Towards(b.Locs.EnemyStart, 4), b.Locs.EnemyStart.Towards(building, 4))
				} else {
					b.Groups.Add(ExpHatchery, building)
				}
			default:
				b.Groups.Add(Buildings, building) // And remove from current group
			}
			continue
		}
		// Cancel building if it will be destroyed soon
		if building.HPS*2.5 > building.Hits {
			building.Command(ability.Cancel)
			b.Groups.Add(MaxGroup, building) // Just to remove from current group
		}
		// Realtime bug test:
		// building.Command(ability.Cancel)
	}
	// todo: Check that it works
	// todo: Don't reorder
	for _, hat := range b.Units.My[zerg.Hatchery].Filter(scl.Ready) {
		if hat.HPS*2.5 > hat.Hits {
			hat.Command(ability.Cancel)
		}
	}
}

func (b *bot) Build() {
	// Move idle or misused builders into miners
	idleBuilder := b.Groups.Get(Builders).Units.First(func(unit *scl.Unit) bool {
		return unit.IsIdle() || unit.IsGathering() || unit.IsReturning()
	})
	if idleBuilder != nil {
		b.Groups.Add(Miners, idleBuilder)
	}
	if b.Units.My[zerg.Hatchery].Empty() {
		return
	}

	// Wait until we have enough minerals to build spawning pool
	if b.CanBuy(ability.Build_SpawningPool) && b.Units.My[zerg.SpawningPool].Empty() &&
		b.Orders[ability.Build_SpawningPool] == 0 && b.Units.My[zerg.Drone].Exists() {
		pos := b.Units.My[zerg.Hatchery].ClosestTo(b.Locs.EnemyStart).Point().Towards(b.Locs.MapCenter, 5)
		if b.Units.My.All().Filter(scl.Structure).CloserThan(math.Sqrt2*2, pos).Exists() {
			pos += point.Pt((rand.Float64())*10, (rand.Float64())*10)
		}
		if b.Enemies.Visible.CloserThan(8, pos).Empty() {
			builder := b.Groups.Get(Miners).Units.Filter(scl.Gathering).ClosestTo(pos)
			if builder != nil {
				b.Groups.Add(Builders, builder)
				pos = b.FindClosestPos(pos, scl.S3x3, scl.Zero, 5, 1, scl.IsBuildable, scl.IsCreep)
				builder.CommandPos(ability.Build_SpawningPool, pos)
				return
			}
		}
	}

	pool := b.Units.My[zerg.SpawningPool].First()
	if pool != nil && b.Units.My[zerg.Extractor].Empty() && b.CanBuy(ability.Build_Extractor) &&
		b.Orders[ability.Build_Extractor] == 0 && b.Units.My[zerg.Drone].Exists() &&
		!b.Upgrades[ability.Research_ZerglingMetabolicBoost] &&
		b.Orders[ability.Research_ZerglingMetabolicBoost] == 0 {
		// Pool exists but there is no extractor and MetabolicBoost isn't researched
		geyser := b.Units.Geysers.All().ClosestTo(b.Locs.MyStart)
		if geyser != nil {
			builder := b.Groups.Get(Miners).Units.Filter(scl.Gathering).ClosestTo(geyser)
			if builder != nil {
				b.Groups.Add(Builders, builder)
				builder.CommandTag(ability.Build_Extractor, geyser.Tag)
				return
			}
		}
	}

	if pool != nil || b.Units.My[zerg.Drone].Len() < 10 {
		// We are building spawning pool (or it is ready) or not enough drones
		poolReady := false
		if pool != nil {
			poolReady = pool.IsReady()
		}

		// Build overlords
		if b.FoodLeft <= 4+b.FoodCap/25 && b.Units.My[zerg.Larva].Exists() &&
			b.CanBuy(ability.Train_Overlord) &&
			b.Orders[ability.Train_Overlord] <= b.FoodCap/50 {
			b.Units.My[zerg.Larva].First().Command(ability.Train_Overlord)
			return
		}

		// Enemy is sieged
		spinesAreCloseToEnemyExp := b.Units.My[zerg.SpineCrawler].CloserThan(12, b.Locs.EnemyExps[0]).Exists()
		spinesAreCloseToEnemyRamp := b.Units.My[zerg.SpineCrawler].CloserThan(12, b.Ramps.Enemy.Top).Exists()
		tooManyResources := b.Minerals > 400
		spinesEnough := b.Units.My[zerg.SpineCrawler].Len() > 4
		// Hat build conditions
		notOrderedToBuild := b.Orders[ability.Build_Hatchery] == 0
		notHatInProgress := b.Units.My[zerg.Hatchery].First(func(unit *scl.Unit) bool { return !unit.IsReady() }) == nil
		hatsSaturated := b.Units.My[zerg.Hatchery].Len() < 10 && b.Units.My[zerg.Hatchery].Sum(func(unit *scl.Unit) float64 {
			return float64(unit.AssignedHarvesters + 2 - unit.IdealHarvesters)
		}) >= 0
		if (spinesAreCloseToEnemyExp || spinesAreCloseToEnemyRamp || tooManyResources || spinesEnough) &&
			notOrderedToBuild && notHatInProgress && hatsSaturated {
			if !b.CanBuy(ability.Build_Hatchery) {
				return // Gather money
			}
			for _, pos := range b.Locs.MyExps {
				if b.Units.My.All().Filter(scl.Structure).CloserThan(6, pos).Exists() {
					continue // todo: better check
				}
				builder := b.Groups.Get(Miners).Units.Filter(scl.Gathering).ClosestTo(pos)
				if builder != nil {
					b.Groups.Add(Builders, builder)
					builder.CommandPos(ability.Build_Hatchery, pos)
					return
				}
			}
		}

		if poolReady {
			// Buy proxy queen
			hat := b.Groups.Get(ProxyHatchery).Units.First()
			if hat != nil && hat.IsReady() && hat.IsIdle() && b.CanBuy(ability.Train_Queen) {
				hat.Command(ability.Train_Queen)
				return
			} else if !workerRush && !b.CanBuy(ability.Train_Queen) &&
				b.Units.My[zerg.Queen].Len()+b.Orders[ability.Train_Queen] < 2 {
				return // Gather money
			}
		}

		hats := b.Units.My[zerg.Hatchery].Filter(scl.Ready)
		for _, hat := range hats {
			larvae := b.Units.My[zerg.Larva].CloserThan(scl.ResourceSpreadDistance, hat.Point())
			enemiesClose := b.Enemies.Visible.CloserThan(8, hat.Point()).Exists()
			if !enemiesClose && (!workerRush || !poolReady) && larvae.Exists() &&
				b.CanBuy(ability.Train_Drone) && b.Orders[ability.Train_Drone] < 3 &&
				b.Units.My[zerg.Drone].Len() < 60 {
				// Build drones
				dronesNeeded := int(hat.IdealHarvesters)
				if b.Units.My[zerg.Zergling].Len() < 6 {
					dronesNeeded -= 4 // To make first 6 lings
				}
				if (!buildSpines && hat.Point().Dist2(b.Locs.MyStart) > 1) || b.Units.My[zerg.Drone].Len() >= 70 {
					dronesNeeded = 0
				}
				if int(hat.AssignedHarvesters) < dronesNeeded {
					larvae.First().Command(ability.Train_Drone)
					return
				}
			}

			// Build zerglings
			if poolReady && larvae.Exists() && b.CanBuy(ability.Train_Zergling) {
				larvae.First().Command(ability.Train_Zergling)
				return
			}
		}

		// Spines
		spines := b.Units.My[zerg.SpineCrawler].Filter(scl.Ready)
		uprooted := b.Units.My[zerg.SpineCrawlerUprooted].Filter(scl.Ready)
		enoughQueens := b.Units.My[zerg.Queen].Len()+b.Orders[ability.Train_Queen] >= 2
		if poolReady && buildSpines && spines.Len()+uprooted.Len() < 8 && enoughQueens {
			// Build more
			hat := b.Groups.Get(ProxyHatchery).Units.First()
			if hat != nil && b.CanBuy(ability.Build_SpineCrawler) && creepPath.Exists() {
				drone := b.Groups.Get(Miners).Units.CloserThan(scl.ResourceSpreadDistance, hat.Point()).
					Filter(scl.Gathering).ClosestTo(b.Locs.EnemyStart)
				if drone != nil {
					pts := b.CrawlerPosesList(spines)
					if pts.Exists() {
						b.Groups.Add(Builders, drone)
						drone.Command(ability.Stop_Stop)
						for _, p := range pts {
							drone.CommandPosQueue(ability.Build_SpineCrawler, p)
						}
					}
				}
			}
		}
		// Spores
		if b.EnemyRace == api.Race_Protoss && b.Loop > scl.TimeToLoop(5, 30) {
			spores := b.Units.My[zerg.SporeCrawler]
			uprooted := b.Units.My[zerg.SporeCrawlerUprooted].Filter(scl.Ready)
			if poolReady && buildSpines && enoughQueens &&
				spores.Len()+uprooted.Len()+b.Orders[ability.Build_SporeCrawler] < 2 {
				// Copypaste
				hat := b.Groups.Get(ProxyHatchery).Units.First()
				if hat != nil && b.CanBuy(ability.Build_SporeCrawler) && creepPath.Exists() {
					drone := b.Groups.Get(Miners).Units.CloserThan(scl.ResourceSpreadDistance, hat.Point()).
						Filter(scl.Gathering).ClosestTo(b.Locs.EnemyStart)
					if drone != nil {
						pts := b.CrawlerPosesList(spores)
						if pts.Exists() {
							b.Groups.Add(Builders, drone)
							drone.Command(ability.Stop_Stop)
							for _, p := range pts {
								drone.CommandPosQueue(ability.Build_SporeCrawler, p)
							}
						}
					}
				}
			}
		}

		if poolReady && (!workerRush || b.Units.My[zerg.Zergling].Len() >= 6) {
			// Buy home queen
			if b.Units.My[zerg.Queen].Empty() && b.CanBuy(ability.Train_Queen) &&
				b.Orders[ability.Train_Queen] == 0 && b.Units.My[zerg.Hatchery].Exists() {
				b.Units.My[zerg.Hatchery].First().Command(ability.Train_Queen)
				return
			}
		}
	}
}

func (b *bot) Builders() {
	builders := b.Groups.Get(Builders).Units
	for _, u := range builders {
		enemy := b.Enemies.Visible.First(func(unit *scl.Unit) bool {
			return unit.InRange(u, 0.5)
		})
		if enemy != nil {
			b.Groups.Add(Miners, u)
		}
	}
}

func (b *bot) RecalcEnemyStartLoc(np point.Point) {
	b.Locs.EnemyStart = np
	b.FindExpansions()
	b.InitRamps()
}

func (b *bot) Scout() {
	drone := b.Groups.Get(Scout).Units.First()
	if b.Locs.EnemyStarts.Len() > 1 && drone == nil && b.Loop < 60 {
		drone = b.Groups.Get(Miners).Units.ClosestTo(b.Locs.EnemyStarts[0])
		if drone != nil {
			b.Groups.Add(Scout, drone)
			drone.CommandPos(ability.Move, b.Locs.EnemyStarts[0])
		}
		return
	}

	if drone != nil {
		if drone.IsIdle() {
			// Check N-1 positions
			for _, p := range b.Locs.EnemyStarts[:b.Locs.EnemyStarts.Len()-1] {
				if b.Grid.IsExplored(p) {
					continue
				}
				drone.CommandPos(ability.Move, p)
				return
			}
			// If N-1 checked and not found, then N is EnemyStartLoc
			b.RecalcEnemyStartLoc(b.Locs.EnemyStarts[b.Locs.EnemyStarts.Len()-1])
			b.Groups.Add(ScoutBase, drone) // promote scout
			return
		}

		if buildings := b.Enemies.Visible.Filter(scl.Structure); buildings.Exists() {
			for _, p := range b.Locs.EnemyStarts[:b.Locs.EnemyStarts.Len()-1] {
				if buildings.CloserThan(20, p).Exists() {
					b.RecalcEnemyStartLoc(p)
					b.Groups.Add(ScoutBase, drone) // promote scout
					return
				}
			}
		}
	}
}

func (b *bot) ScoutBase() {
	if b.Loop > 2688 { // 2:00
		return
	}

	if b.Loop < 1568 && b.Units.AllEnemy[terran.Barracks].Len() >= 2 { // 1:10
		safeMode = true
	}

	drone := b.Groups.Get(ScoutBase).Units.First()
	// don't scout
	/*if b.Locs.EnemyStarts.Len() <= 1 && drone == nil && b.Loop < 60 {
		drone = b.Groups.Get(Miners).Units.ClosestTo(b.Locs.EnemyStart)
		if drone != nil {
			b.Groups.Add(ScoutBase, drone)
		}
	}*/
	if drone == nil {
		return
	}

	enemies := b.Enemies.All.Filter(scl.DpsGt5)
	if enemies.Exists() || b.Loop > 2016 { // 1:30
		b.Groups.Add(Miners, drone) // dismiss scout
	}
	if b.EnemyRace == api.Race_Zerg && b.CanBuy(ability.Build_SpineCrawler) &&
		b.Units.My[zerg.SpawningPool].First(scl.Ready) != nil {
		mfs := b.Units.Minerals.All().CloserThan(scl.ResourceSpreadDistance, b.Locs.EnemyStart)
		if mfs.Exists() {
			b.Groups.Add(Builders, drone)
			pos := mfs.Center().Towards(b.Locs.EnemyStart, -4)
			pos = b.FindClosestPos(pos, scl.S2x2, scl.Zero, 3, 1, scl.IsBuildable, scl.IsCreep)
			// drone.CommandPos(ability.Move, pos)
			drone.CommandPos(ability.Build_SpineCrawler, pos)
			return
		}
	}

	vec := (drone.Point() - b.Locs.EnemyStart).Norm().Rotate(math.Pi / 10)
	pos := b.Locs.EnemyStart + vec*10
	drone.CommandPos(ability.Move, pos)
}

func (b *bot) ProxyBuilder() {
	if b.Loop > scl.TimeToLoop(1, 10) && !workerRush { // 1:10 sec && not defending from worker rush
		drone := b.Groups.Get(ProxyBuilder).Units.First()
		if drone == nil && b.Units.My[zerg.Hatchery].Len() < 2 {
			drone := b.Groups.Get(Miners).Units.Filter(scl.Gathering).ClosestTo(b.Locs.EnemyStart)
			if drone != nil {
				b.Groups.Add(ProxyBuilder, drone)
				drone.CommandPos(ability.Move, b.Locs.EnemyExps[takeExp])
			}
		}
	}
	if b.Loop > scl.TimeToLoop(1, 10) {
		drone := b.Groups.Get(ProxyBuilder).Units.First()
		if drone != nil && (drone.TargetAbility() != ability.Build_Hatchery ||
			b.Enemies.All.CloserThan(safeBuildRange, drone.Point()).Exists()) {
			safeExpLoc := b.Locs.EnemyExps[takeExp+1]
			for _, exp := range b.Locs.EnemyExps[takeExp:] {
				if b.Enemies.All.CloserThan(safeBuildRange, exp).Empty() {
					safeExpLoc = exp
					break
				}
			}
			if b.CanBuy(ability.Build_Hatchery) && drone.Point().IsCloserThan(3, safeExpLoc) {
				drone.CommandPos(ability.Build_Hatchery, safeExpLoc)
			} else if drone.IsFarFrom(safeExpLoc) {
				drone.CommandPos(ability.Move, safeExpLoc)
			}
		}
	}
}

func (b *bot) FindLastCreepCell() int {
	l := creepPath.Len()
	for k, p := range creepPath {
		if !b.Grid.IsCreep(p) {
			return k - 1
		}
		if k == l-1 {
			return k
		}
	}
	return -1
}

func (b *bot) FindLastCreepCellPoint() point.Point {
	if key := b.FindLastCreepCell(); key > 0 {
		// Add some random to not place tumors always on the edge
		for key > 0 {
			if rand.Float64() > 0.5 {
				key--
			} else {
				break
			}
		}
		return creepPath[key]
	}
	return 0
}

func (b *bot) Overs() {
	overs := b.Groups.Get(Overs).Units
	if overs.Empty() {
		return
	}
	if creepPath.Exists() {
		lccn := b.FindLastCreepCell()
		if lccn < 0 {
			return
		}
		lcc := creepPath[lccn]
		spines := b.Units.My.OfType(zerg.SpineCrawler, zerg.SpineCrawlerUprooted)
		if spines.Exists() {
			spines.OrderBy(func(unit *scl.Unit) float64 {
				return unit.Point().Dist2(lcc) + 1/float64(unit.Tag)
			}, false)
			overs.OrderBy(scl.CmpTags, false)
			for k, spine := range spines {
				if k >= overs.Len()-2 {
					break
				}
				np, _ := overs[k].AirEvade(b.Enemies.Visible, 1, spine.Point())
				if np.IsFurtherThan(1, overs[k].Point()) {
					overs[k].CommandPos(ability.Move, np)
				}
			}
			for k := overs.Len() - 2; k < overs.Len(); k++ {
				if overs[k].HPS > 0 {
					np, _ := overs[k].AirEvade(b.Enemies.Visible, 1, overs[k].Point())
					if np.IsFurtherThan(1, overs[k].Point()) {
						overs[k].CommandPos(ability.Move, np)
						continue
					}
				}
				if overs[k].IsIdle() {
					pos := b.Locs.EnemyExps[rand.Intn(len(b.Locs.EnemyExps))]
					overs[k].CommandPos(ability.Move, pos)
				}
			}
		}
	} else {
		if overs.Len() == 1 {
			over := overs.First()
			// pos := b.Locs.EnemyExps[takeExp]
			pos := b.Ramps.Enemy.Top - b.Ramps.Enemy.Vec*6
			if over.HPS == 0 && over.IsFarFrom(pos) {
				over.CommandPos(ability.Move, pos)
				return
			}
		}
		if overs.Len() == 2 {
			over := overs.ClosestTo(b.Locs.MyStart)
			pos := b.Locs.MyExps[0]
			if over.HPS == 0 && over.IsFarFrom(pos) {
				over.CommandPos(ability.Move, pos)
				return
			}
		}
		for _, over := range overs {
			// Evade enemy antiair
			np, _ := over.AirEvade(b.Enemies.Visible, 1, over.Point())
			if np.IsFurtherThan(1, over.Point()) {
				over.CommandPos(ability.Move, np)
			}
		}
	}
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func (b *bot) Tumors() {
	tumors := b.Groups.Get(ActiveTumors).Units
	if tumors.Empty() {
		return
	}
	if creepPath.Empty() {
		return
	}
	for _, tumor := range tumors {
		if tumor.HasAbility(ability.Build_CreepTumor_Tumor) {
			lcc := b.FindLastCreepCell()
			if lcc < 8 {
				return
			}
			for x := min(lcc+10, creepPath.Len()-1); x > 0; x-- {
				if tumor.Point().IsCloserThan(10, creepPath[x]) {
					tumor.CommandPosQueue(ability.Build_CreepTumor_Tumor, creepPath[x])
				}
			}
			b.Groups.Add(UsedTumors, tumor)
			return
		}
	}
}

func (b *bot) FindCrawlerPos(pos, vec point.Point, amount int) point.Points {
	var pts point.Points
	buildings := b.Units.My.All().Filter(scl.Structure)
	enemies := b.Enemies.All.Filter(func(unit *scl.Unit) bool {
		return unit.GroundDPS() > 5 && unit.Point().IsCloserThan(20, pos)
	})
	for y := float64(1); y <= 12; y++ {
		for x := float64(1); x <= y; x++ {
			for _, s := range []float64{-1, 1} {
				p := (pos + vec.Mul(y-x) + (vec * 1i).Mul(s*(x+1))).Floor()
				if b.IsPosOk(p, scl.S2x2, scl.Zero, scl.IsBuildable, scl.IsCreep) &&
					buildings.CloserThan(math.Sqrt2, p).Empty() && enemies.CloserThan(safeBuildRange, p).Empty() {
					pts.Add(p)
					amount--
					if amount <= 0 {
						return pts
					}
				}
			}
		}
	}
	return pts
}

func (b *bot) CrawlerPosesList(spines scl.Units) point.Points {
	enemies := b.Enemies.All.Filter(scl.DpsGt5)
	buildings := b.Units.My.OfType(zerg.Hatchery, zerg.SpineCrawler, zerg.SporeCrawler)
	// Start from base, stop if it is last cell on creep
	lkc := b.FindLastCreepCell()
	spinesDist := math.Inf(1)
	if spines.Exists() {
		spinesDist = spines.ClosestTo(b.Locs.EnemyStart).Point().Dist2(b.Locs.EnemyStart)
	}
	for kc := 1; kc <= lkc; kc++ {
		pos := creepPath[kc]
		if enemies.CloserThan(safeBuildRange-2, pos).Empty() && kc != lkc &&
			(pos.Dist2(b.Locs.EnemyStart) > spinesDist || buildings.CloserThan(safeBuildRange+2, pos).Exists()) {
			continue // Stop if enemies are detected on the way to creep edge
		}

		vec := creepPath[kc-1] - creepPath[kc]
		return b.FindCrawlerPos(pos, vec, 20)
	}
	return nil
}

func (b *bot) Crawlers() {
	crawlers := b.Units.My.OfType(zerg.SpineCrawler, zerg.SporeCrawler).Filter(scl.Ready)
	uprooted := b.Units.My.OfType(zerg.SpineCrawlerUprooted, zerg.SporeCrawlerUprooted).Filter(scl.Ready)
	if crawlers.Len() >= 4 && crawlers.Len()/2 > uprooted.Len() {
		lcc := b.FindLastCreepCellPoint()
		if lcc != 0 {
			crawlers.OrderBy(func(unit *scl.Unit) float64 {
				return unit.Point().Dist2(lcc)
			}, true)
			crawler := crawlers.First(func(unit *scl.Unit) bool {
				return b.Enemies.Visible.InRangeOf(unit, 3).Empty()
			})
			if crawler != nil {
				if crawler.UnitType == zerg.SpineCrawler {
					crawler.Command(ability.Morph_SpineCrawlerUproot)
				} else {
					crawler.Command(ability.Morph_SporeCrawlerUproot)
				}
			}
		}
	}
	if uprooted.Len() > 0 {
		crawler := uprooted.First(scl.Idle)
		if crawler != nil {
			pts := b.CrawlerPosesList(crawlers)
			for _, p := range pts {
				if crawler.UnitType == zerg.SpineCrawlerUprooted {
					crawler.CommandPosQueue(ability.Morph_SpineCrawlerRoot, p)
				} else {
					crawler.CommandPosQueue(ability.Morph_SporeCrawlerRoot, p)
				}
			}
		}

		for _, crawler := range uprooted {
			enemies := b.Enemies.Visible.CanAttack(crawler, -2)
			if enemies.Exists() || crawler.HPS > 0 {
				crawler.CommandPos(ability.Move, crawlers.Center())
			}
		}
	}
	for _, crawler := range crawlers {
		if crawler.WeaponCooldown == 0 {
			continue
		}
		enemies := b.Enemies.Visible.Filter(scl.Visible).InRangeOf(crawler, 0)
		if enemies.Exists() {
			crawler.Attack(enemies)
		}
	}
}

func (b *bot) FindSafePos() point.Point {
	safePos := b.Ramps.My.Top
	if hat := b.Units.My[zerg.Hatchery].ClosestTo(b.Locs.EnemyStart); hat != nil {
		safePos = hat.Point().Towards(b.Locs.EnemyStart, -5)
	}
	if spines := b.Units.My[zerg.SpineCrawler].Filter(scl.Ready); spines.Exists() {
		safePos = spines.Center().Towards(b.Locs.EnemyStart, -5)
	}
	return safePos
}

func (b *bot) Queens() {
	hat := b.Groups.Get(ProxyHatchery).Units.First()
	safePos := b.FindSafePos()
	queens := b.Groups.Get(Queens).Units
	endangered := queens.Filter(func(unit *scl.Unit) bool { return float64(unit.HealthMax)-unit.Hits >= 125 })
	if endangered.Empty() {
		endangered = b.Units.My.OfType(zerg.SpineCrawler, zerg.SpineCrawlerUprooted,
			zerg.SporeCrawler, zerg.SporeCrawlerUprooted).
			Filter(func(unit *scl.Unit) bool { return float64(unit.HealthMax)-unit.Hits >= 125 })
	}

	okTargets := scl.Units{}
	goodTargets := scl.Units{}
	for _, unit := range b.Enemies.Visible {
		if unit.IsNot(zerg.Larva, zerg.Egg, protoss.AdeptPhaseShift, terran.KD8Charge) {
			if unit.GroundRange() < 5 || unit.IsDefensive() {
				okTargets.Add(unit)
			}
			if unit.IsFlying {
				goodTargets.Add(unit)
			}
		}
	}

	for _, u := range queens {
		enemies := b.Enemies.All.CanAttack(u, 0)
		targets := b.Enemies.All.InRangeOf(u, 0)
		if enemies.Exists() || u.HPS > 0 {
			if u.IsFarFrom(safePos) && u.IsCoolToMove() {
				u.SpamCmds = true
				u.CommandPos(ability.Move, safePos)
				continue
			}
		}
		if targets.Exists() {
			u.Attack(targets)
			continue
		}

		if u.Energy >= 25 {
			if u.TargetAbility() == ability.Build_CreepTumor_Queen ||
				u.TargetAbility() == ability.Effect_InjectLarva {
				continue
			}
			if b.Groups.Get(ActiveTumors).Units.Len() < 2 {
				if p := b.FindLastCreepCellPoint(); p != 0 {
					hazards := b.Enemies.All.Filter(scl.DpsGt5)
					if hazards.CloserThan(safeBuildRange, p).Empty() {
						u.CommandPos(ability.Build_CreepTumor_Queen, p)
						continue
					}
				}
			}
			if hat != nil && !hat.HasBuff(buff.QueenSpawnLarvaTimer) {
				u.CommandTag(ability.Effect_InjectLarva, hat.Tag)
				continue
			}
		}
		if u.Energy >= 50 {
			if u.TargetAbility() == ability.Build_CreepTumor_Queen ||
				u.TargetAbility() == ability.Effect_InjectLarva ||
				u.TargetAbility() == ability.Effect_Transfusion {
				continue
			}
			if endangered.Exists() {
				u.CommandTag(ability.Effect_Transfusion, endangered.ClosestTo(u.Point()).Tag)
				continue
			}
		}

		if armyAttack && (goodTargets.Exists() || okTargets.Exists()) {
			u.Attack(goodTargets, okTargets)
		} else if u.Point().IsFurtherThan(u.SightRange()/2, safePos) {
			u.CommandPos(ability.Move, safePos)
		}
	}
}

func (b *bot) HomeQueen() {
	queen := b.Groups.Get(HomeQueen).Units.First(func(unit *scl.Unit) bool {
		return unit.Energy >= 25 // Ready for injection
	})
	if queen != nil {
		hat := b.Groups.Get(HomeHatchery).Units.First()
		if hat != nil {
			queen.CommandTag(ability.Effect_InjectLarva, hat.Tag)
		}
	}
}

func (b *bot) WorkerRushDefence() {
	enemies := b.Units.Enemy.OfType(terran.SCV, zerg.Drone, protoss.Probe).CloserThan(10, b.Locs.MyStart)
	alert := enemies.CloserThan(6, b.Locs.MyStart).Exists()

	army := b.Groups.Get(WorkerRushDefenders).Units
	if army.Exists() && enemies.Empty() {
		b.Groups.Add(Miners, army...)
		return
	}

	if enemies.Len() >= 10 {
		workerRush = true
	}

	balance := 1.0 * float64(army.Len()) / float64(enemies.Len())
	if alert && balance < 1 {
		worker := b.Groups.Get(Miners).Units.First(scl.Gathering, func(unit *scl.Unit) bool {
			return unit.Hits >= 20
		})
		if worker != nil {
			army.Add(worker)
			b.Groups.Add(WorkerRushDefenders, worker)
		}
	}

	for _, unit := range army {
		if unit.Hits < 11 {
			b.Groups.Add(Miners, unit)
			continue
		}
		unit.Attack(enemies)
	}
}

func (b *bot) Mining() {
	ratio := 0.0
	if b.Vespene < 100 && !b.Upgrades[ability.Research_ZerglingMetabolicBoost] &&
		b.Orders[ability.Research_ZerglingMetabolicBoost] == 0 {
		ratio = 100 // Extract using all workers
		extractor := b.Units.My[zerg.Extractor].Filter(scl.Ready, func(unit *scl.Unit) bool {
			return unit.VespeneContents > 0
		}).First()
		if extractor != nil && len(b.Miners.GasForMiner) < 3 {
			// Rebalance miners
			b.RedistributeWorkersToRefineryIfNeeded(extractor, b.Groups.Get(Miners).Units, 3)
		}
	}
	if b.Vespene >= 100 || b.Upgrades[ability.Research_ZerglingMetabolicBoost] ||
		b.Orders[ability.Research_ZerglingMetabolicBoost] > 0 && len(b.Miners.GasForMiner) > 0 {
		// Enough of gas, free workers
		for minerTag := range b.Miners.GasForMiner {
			delete(b.Miners.CCForMiner, minerTag) // todo: move this logic to lib
			delete(b.Miners.MineralForMiner, minerTag)
		}
	}
	enemies := b.Enemies.Visible.Filter(scl.DpsGt5)
	b.HandleMiners(b.Groups.Get(Miners).Units, b.Units.My[zerg.Hatchery].Filter(scl.Ready),
		enemies, ratio, b.Locs.MyStart-b.Locs.MyStartMinVec*3)
}

func (b *bot) OrderUpgrades() {
	pool := b.Units.My[zerg.SpawningPool].Filter(scl.Ready).First()
	if pool != nil && !b.Upgrades[ability.Research_ZerglingMetabolicBoost] &&
		pool.HasIrrAbility(ability.Research_ZerglingMetabolicBoost) &&
		b.CanBuy(ability.Research_ZerglingMetabolicBoost) {
		pool.Command(ability.Research_ZerglingMetabolicBoost)
	}
}

func (b *bot) IsTreat(unit *scl.Unit, buildings scl.Units) bool {
	for _, u := range buildings {
		if u.IsCloserThan(safeBuildRange+float64(u.Radius+unit.Radius), unit) {
			return true
		}
	}
	return false
}

func LingMoveFunc(u *scl.Unit, target *scl.Unit) {
	// Unit need to be closer to the target to shoot?
	if !u.InRange(target, -0.1) || !target.IsVisible() || !target.IsPosVisible() {
		npos := u.Towards(target, 2)
		effects := []api.EffectID{effect.PsiStorm, effect.CorrosiveBile}
		if !u.IsFlying {
			effects = append(effects, effect.LiberatorDefenderZoneSetup, effect.LiberatorDefenderZone)
		}
		pos, safe := u.EvadeEffectsPos(npos, true, effects...)
		if u.WeaponCooldown > 0 && u.PosDelta == 0 {
			u.SpamCmds = true // Spamming this thing is the key. Or orders will be ignored (or postponed)
		}
		if u.IsCoolToMove() {
			if safe {
				// Move closer
				u.CommandPos(ability.Move, target)
			} else {
				u.CommandPos(ability.Move, pos)
			}
		}
	}
}

func (b *bot) Army() {
	army := b.Groups.Get(Lings).Units
	buildings := b.Units.My.OfType(zerg.Hatchery, zerg.SpawningPool, zerg.SpineCrawler)
	var mfsPos, basePos point.Point
	// For exp recon before 2:40
	if b.Loop < 3600 {
		mfsPos = b.Units.Minerals.All().CloserThan(scl.ResourceSpreadDistance, b.Locs.EnemyExps[0]).Center()
		basePos = b.Units.Minerals.All().CloserThan(scl.ResourceSpreadDistance, b.Locs.EnemyStart).Center()
	}

	if army.Exists() {
		safePos := b.FindSafePos()
		safeRange := safePos.Dist(b.Locs.EnemyStart) + 5
		if b.Enemies.All.Exists() {
			safeRange = math.Max(safeRange, b.Enemies.All.ClosestTo(b.Locs.MyStart).Point().Dist(b.Locs.EnemyStart)+5)
		}
		attackRange := 10.0
		enemyStructures := b.Enemies.Visible.Filter(scl.Structure)
		if enemyStructures.Exists() {
			attackRange = enemyStructures.ClosestTo(b.Locs.MyStart).Point().Dist(b.Locs.MyStart) - 20
		}

		var armyValue, enemyValue float64
		for _, unit := range army.CloserThan(safeRange, b.Locs.EnemyStart) {
			armyValue += unit.GroundDPS() * unit.Hits
		}
		enemies := b.Enemies.All.Filter(scl.Ready)
		for _, unit := range enemies.Filter(func(unit *scl.Unit) bool { return !unit.IsWorker() }) {
			enemyValue += unit.GroundDPS() * unit.Hits
		}
		if armyValue/1.5 > enemyValue || army.Len() >= 50 {
			armyAttack = true
		}
		if armyValue*1.5 < enemyValue && army.Len() < 25 {
			armyAttack = false
		}
		if enemyValue > 2800 && !buildSpines {
			// b.ChatSend("buildSpines")
			buildSpines = true
		}
		// log.Info(armyValue, enemyValue)

		okTargets := scl.Units{}
		goodTargets := scl.Units{}
		for _, unit := range b.Enemies.All {
			if unit.Is(zerg.Larva, zerg.Egg, protoss.AdeptPhaseShift, terran.KD8Charge) || unit.IsFlying {
				continue
			}
			if armyAttack || b.IsTreat(unit, buildings) || unit.Point().IsCloserThan(attackRange, b.Locs.MyStart) {
				okTargets.Add(unit)
			}
			/*if unit.Is(terran.Reaper) {
				continue // Low priority for lings
			}*/
			if (!unit.IsStructure() || unit.Point().IsCloserThan(70, b.Locs.MyStart)) || unit.IsDefensive() {
				// IsVisible - commands optimization, you can't attack tag of unit on hidden ground
				// But unit.IsVisible() could be true! If it is attacking from hidden highground
				if armyAttack || b.IsTreat(unit, buildings) || unit.Point().IsCloserThan(attackRange, b.Locs.MyStart) {
					goodTargets.Add(unit)
				}
			}
		}

		for _, u := range army {
			fasterEnemies := scl.Units{}
			if u.WeaponCooldown == 0 {
				fasterEnemies = enemies.CanAttack(u, 1).Filter(func(unit *scl.Unit) bool {
					return b.U.Types[unit.UnitType].MovementSpeed > b.U.Types[u.UnitType].MovementSpeed
				})
			}

			if u.Hits < 11 && fasterEnemies.Empty() {
				b.Groups.Add(Retreat, u)
				continue
			}

			closeTargets := goodTargets.InRangeOf(u, 7)
			if mfsPos != 0 && !b.Grid.IsExplored(mfsPos) && closeTargets.Empty() {
				u.CommandPos(ability.Move, mfsPos)
			} else if basePos != 0 && !b.Grid.IsExplored(basePos) && closeTargets.Empty() {
				u.CommandPos(ability.Move, basePos)
			} else if goodTargets.Exists() || okTargets.Exists() {
				// b.PriorityAttack(append(goodTargets, fasterEnemies...), okTargets, u)
				u.AttackCustom(scl.DefaultAttackFunc, LingMoveFunc, fasterEnemies, goodTargets, okTargets)
			} else if armyAttack {
				if !b.Grid.IsExplored(b.Locs.EnemyStart) {
					u.CommandPos(ability.Move, b.Locs.EnemyStart)
				} else {
					// Search for other bases
					if u.IsIdle() {
						pos := b.Locs.EnemyExps[rand.Intn(len(b.Locs.EnemyExps))]
						u.CommandPos(ability.Move, pos)
					}
				}
			} else {
				// Gather at safe point
				if u.Point().IsFurtherThan(u.SightRange()/2, safePos) {
					u.CommandPos(ability.Move, safePos)
				}
			}
		}
	}
}

func (b *bot) Retreat() {
	army := b.Groups.Get(Retreat).Units
	if army.Exists() {
		safePos := b.FindSafePos()
		enemiesNear := b.Enemies.All.CloserThan(safeBuildRange, safePos).Exists()
		for _, u := range army {
			if u.Hits > 30 || enemiesNear {
				b.Groups.Add(Lings, u)
				continue
			}
			if b.U.UnitsOrders[u.Tag].Loop+b.FramesPerOrder > b.Loop {
				continue // Not more than FramesPerOrder
			}
			enemies := b.Enemies.All.InRangeOf(u, 0)
			if enemies.Exists() && u.IsCoolToAttack() {
				u.Attack(enemies)
				continue
			}
			if u.IsFarFrom(safePos) && u.IsCoolToMove() {
				if u.WeaponCooldown > 0 {
					// Spamming this thing is the key. Or orders will be ignored (or postponed)
					u.SpamCmds = true
				}
				u.CommandPos(ability.Move, safePos)
			}
		}
	}
}

func (b *bot) Logic() {
	// time.Sleep(time.Millisecond * 5)

	b.BuildingsCheck()
	b.Builders()
	b.Build()
	b.Scout()
	b.ScoutBase()
	b.WorkerRushDefence()
	b.Mining()
	b.OrderUpgrades()
	b.ProxyBuilder()
	b.Overs()
	b.Tumors()
	b.Crawlers()
	b.Queens()
	b.HomeQueen()
	b.Army()
	b.Retreat()
}
