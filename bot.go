package main

import (
	log "bitbucket.org/aisee/minilog"
	"github.com/aiseeq/s2l/helpers"
	"github.com/aiseeq/s2l/lib/scl"
	"github.com/aiseeq/s2l/protocol/api"
)

type bot struct {
	*scl.Bot

	VersionPosted bool
}

const version = "CheeZerg v1.7.2 (glhf)"

// OnStep is called each game step (every game update by default)
func (b *bot) Step() {
	defer helpers.RecoverPanic()

	b.Cmds = &scl.CommandsStack{}
	b.ParseObservation()

	b.Loop = int(b.Obs.GameLoop)
	if b.Loop >= 9 && !b.VersionPosted {
		b.Actions.ChatSend(version, api.ActionChat_Broadcast)
		b.VersionPosted = true
	}
	if b.Loop < b.LastLoop+b.FramesPerOrder {
		return // Skip frame repeat
	} else {
		b.LastLoop = b.Loop
	}

	b.ParseUnits()
	b.ParseOrders()
	b.DetectEnemyRace()

	b.Logic()

	b.Cmds.Process(&b.Actions)
	if len(b.Actions) > 0 {
		// log.Info(b.Loop, len(b.Actions), b.Actions)
		if resp, err := b.Client.Action(api.RequestAction{Actions: b.Actions}); err != nil {
			log.Error(err)
		} else {
			_ = resp.Result // todo: do something with it?
		}
		b.Actions = nil
	}

	// b.DebugEnemyUnits()
	// b.DebugSend()
}
